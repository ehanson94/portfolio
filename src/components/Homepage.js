import React from 'react';
import headshot from '../images/headshot.jpeg';
import PageHeader from './PageHeader';
import About from './About';
import Contact from './Contact';
import Projects from './Projects';
import '../App.css';

export default function Homepage() {
    return(
        <React.Fragment>
            <PageHeader></PageHeader>
            <img src={headshot} alt="Eric Hanson" id='headshot'></img>
            <div id="cards">
                <About></About>
                <Contact></Contact>
            </div>
            <Projects></Projects>
        </React.Fragment>
    )
}