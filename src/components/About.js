import React from 'react';
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle
} from 'shards-react';

import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css";

export default function About() {
    return(
        <Card id="aboutCard">
            <CardHeader className='cardheader'>
                <CardTitle>A Little Bit About Me!</CardTitle>
            </CardHeader>
            <CardBody>
                Hello! My name is Eric Hanson and I am a Junior Software Developer living in Indianapolis, IN! I graduated with a Front-End Developer Certificate from Kenzie Academy. I am currently working for the school as a Software Engineering Facilitator since January 2020. I cover material on HTML, CSS, and Javascript. I am very excited to start my career as a Junior Developer! In my free time, I love getting outdoors to find new and interesting places and activities. I love mountain biking, rock climbing, biking, longboarding, swimming, solving escape rooms, trying new restaurants, kayaking, and playing video games! I have one dog named Marley. I would love to find a company where I feel like I can make a real impact on people's lives. I love helping people by teaching, but I know that I want to go out and gain work experience until I feel ready to move back into teaching. I hope you enjoy the projects that I have provided!
            </CardBody>
        </Card>
    )
}