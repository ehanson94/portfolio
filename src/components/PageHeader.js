import React from "react";
import { Navbar, Nav, NavLink, NavItem } from "shards-react";

import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css";

export default function PageHeader() {
  return (
    <React.Fragment>
      <div id="header">
        <h1>Eric Hanson</h1>
      </div>
      <Navbar id="nav">
        <Nav>
          <NavItem>
            <NavLink active href="/">
              Home
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink active href="https://docs.google.com/document/d/19sPQlV8qk-pY5_aLH8lT-1SrSly3OVqw4iuEorRhQgk/edit?usp=sharing">
              Resume
            </NavLink>
          </NavItem>
        </Nav>
      </Navbar>
    </React.Fragment>
  );
}
