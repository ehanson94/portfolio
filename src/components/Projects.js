import React from 'react';
import { Card, CardBody, CardImg, CardSubtitle } from "shards-react";

import photosharing from "../images/photosharing.png";
import kwitterapi from "../images/kwitterapi.png";
import photowall from '../images/photowall.png';
import combatgame from '../images/combatgame.png';
import capstone from '../images/capstone.png';

export default function Projects() {
    return (
        <React.Fragment>
    
          <div id="projects">
            <Card className="projectcards">
              <CardBody>
                <CardSubtitle>
                  <a href="https://photo-sharing-service.herokuapp.com/">
                    Photo Sharing Site
                  </a>
                </CardSubtitle>
                This is a mockup of a simple photo sharing site.
              </CardBody>
              <CardImg bottom src={photosharing} />
            </Card>
    
            <Card className="projectcards">
              <CardBody>
                <CardSubtitle>
                  <a href="https://capsule-corp.herokuapp.com/docs/">
                    Capsule Corp API
                  </a>
                </CardSubtitle>
                This was a site that was set up like Twitter. This is is the API
                that we built out to handle the requests from the site we made.
              </CardBody>
              <CardImg bottom src={kwitterapi} />
            </Card>
    
            <Card className="projectcards">
              <CardBody>
                <CardSubtitle>
                  <a href="https://ehanson94.gitlab.io/assessment---react-photo-wall/">
                    Photo Wall
                  </a>
                </CardSubtitle>
                In this project, we set up a react app that would pull images from an API and display them to the page when you press the button.
              </CardBody>
              <CardImg bottom src={photowall} />
            </Card>
    
            <Card className="projectcards">
              <CardBody>
                <CardSubtitle>
                  <a href="https://ehanson94.gitlab.io/combat-game3/">
                    Combat Game
                  </a>
                </CardSubtitle>
                I made a simple, turn-based combat game!
              </CardBody>
              <CardImg bottom src={combatgame} />
            </Card>
    
            <Card className="projectcards">
              <CardBody>
                <CardSubtitle>
                  <a href="https://gitlab.com/SmileySlays/capstone-boilerplate">
                    Capstone Project
                  </a>
                </CardSubtitle>
                For my Front-End Capstone project, we built an app to keep track of a collector's Magic: The Gathering cards. It gave you the ability to log in as either a store owner or a collector. Store owners can log all of their cards in their inventory, for collectors to view and purchase for pick up at their store. Collectors can keep track of their own cards and can search store inventories for cards they would like to purchase. I worked on this project in a group of three other students.
              </CardBody>
              <CardImg bottom src={capstone} />
            </Card>
          </div>
        </React.Fragment>
      );
}