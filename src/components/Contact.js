import React from 'react';
import {
    Card,
    CardTitle,
    CardBody,
    CardHeader
} from 'shards-react';

import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css";

export default function Contact() {
    return(
        <Card id='contact'>
            <CardHeader className='cardheader'>
                <CardTitle>Contact Me</CardTitle>
            </CardHeader>
            <CardBody>
                <p>
                    Email: <a href='mailto:ehanson94@gmail.com'>ehanson94@gmail.com</a>
                </p>
                <p>
                    Phone: <a href='tel:765-465-9204'>1 (765)465-9204</a>
                </p>
                <p>
                    LinkedIn: <a href='https://www.linkedin.com/in/eric-hanson-1a1568155/'>Let's Connect!</a>
                </p>
                <p>
                    Github: <a href='https://github.com/ericmhanson'>ericmhanson</a>
                </p>
                <p>
                    Gitlab: <a href='https://gitlab.com/ehanson94'>ehanson94</a>
                </p>
            </CardBody>
        </Card>
    )
}