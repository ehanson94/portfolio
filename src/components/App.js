import React from "react";
import { BrowserRouter, Link } from "react-router-dom";
import "../App.css";

import Homepage from './Homepage';

export default function App() {
  return (
      <BrowserRouter>
        <Link to="/" component={Homepage} exact />
      </BrowserRouter>
  );
}
